package com.mmh.dev.core.android;

import android.app.Application;
import android.content.Context;


import com.mmh.dev.R;
import com.mmh.dev.core.di.components.AppComponent;
import com.mmh.dev.core.di.modules.ApiModule;
import com.mmh.dev.core.di.modules.AppModule;
import com.mmh.dev.core.di.modules.DataModule;
import com.mmh.dev.core.di.modules.MappersModule;
import com.mmh.dev.core.di.modules.ThreadExecutorsModule;

import net.danlew.android.joda.JodaTimeAndroid;


/**
 */
public class App extends Application{

    private AppComponent appComponent;

    public static App getApp(Context context) {
        return (App) context.getApplicationContext();
    }

    public AppComponent getAppComponent(){
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//             This process is dedicated to LeakCanary for heap analysis.
//             You should not init your app in this process.
//            return;
//        }
//        LeakCanary.install(this);
//        Fabric.with(this, new Crashlytics(), new Answers());
        JodaTimeAndroid.init(this);
        setupAppComponent();
        setupPicasso();
    }

    private void setupAppComponent(){
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .apiModule(new ApiModule(getString(R.string.baseUrl)))
                .threadExecutorsModule(new ThreadExecutorsModule())
                .dataModule(new DataModule())
                .mappersModule(new MappersModule())
                .build();
    }

    private void setupPicasso(){

    }
}
