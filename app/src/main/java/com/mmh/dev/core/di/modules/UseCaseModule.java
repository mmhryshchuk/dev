package com.mmh.dev.core.di.modules;


import dagger.Module;


/**
 */
@Module(includes = {
        RepositoriesModule.class,
        ThreadExecutorsModule.class,
        ApiModule.class
})
public class UseCaseModule {



}
