package com.mmh.dev.core.di.components;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Subcomponent;

/**
 */
@Subcomponent(modules = {ServicesComponent.ServicesModule.class})
@Singleton
public interface ServicesComponent {


    void inject();

    @Module
    class ServicesModule {
    }
}
