package com.mmh.dev.core.di.modules;

import com.mmh.dev.core.android.App;
import com.mmh.dev.domain.repository.PreferenceRepository;
import com.mmh.dev.domain.repository.SaveEntityRepository;
import com.mmh.dev.domain.repository.impl.PreferenceRepositoryImpl;
import com.mmh.dev.domain.repository.impl.SaveEntityRepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;




/**
 */
@Module(includes = {
        DataModule.class,
        MappersModule.class
})
public class RepositoriesModule {

    @Provides
    @Singleton
    PreferenceRepository providePreferenceRepository(App app){
        return new PreferenceRepositoryImpl(app);
    }
    @Provides
    @Singleton
    SaveEntityRepository provideSaveEntityRepo(){
        return new SaveEntityRepositoryImpl();
    }


}
