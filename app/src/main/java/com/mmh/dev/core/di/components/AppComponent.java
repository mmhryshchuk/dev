package com.mmh.dev.core.di.components;

import com.mmh.dev.core.android.App;
import com.mmh.dev.core.bus.Bus;
import com.mmh.dev.core.di.modules.ApiModule;
import com.mmh.dev.core.di.modules.AppModule;
import com.mmh.dev.core.di.modules.DataModule;
import com.mmh.dev.core.di.modules.MappersModule;
import com.mmh.dev.core.di.modules.RepositoriesModule;
import com.mmh.dev.core.di.modules.ThreadExecutorsModule;
import com.mmh.dev.core.di.modules.UseCaseModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 */
@Singleton
@Component(modules = {
        AppModule.class,
        DataModule.class,
        ThreadExecutorsModule.class,
        ApiModule.class,
        UseCaseModule.class,
        RepositoriesModule.class,
        MappersModule.class
})
public interface AppComponent {
    App getApp();
    Bus getBus();


}
