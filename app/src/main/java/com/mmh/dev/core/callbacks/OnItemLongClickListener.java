package com.mmh.dev.core.callbacks;

/**
 */

public interface OnItemLongClickListener<Data> {
    void onLongClicked(Data data);
}
