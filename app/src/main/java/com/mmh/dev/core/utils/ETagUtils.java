package com.mmh.dev.core.utils;

import okhttp3.Headers;
import retrofit2.Response;

/**
 */
public class ETagUtils {

    public static String parseEtag(Response response){
        Headers headers = response.headers();
        return headers.get("ETag");
    }

}
